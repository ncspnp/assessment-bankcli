# # Backend Assessment - BankCLI

For this challenge you're going to create a Bank Command Line Interface, this will give us an idea about your coding skills

### Bank Command Line Interface

Develop command line interface (CLI) to simulate interaction with a retail bank. 

Implement the  following commands:


| Command | Description |
| --------------------------------- | -------------------------------------------------------------------------------------------- |
| login `<client>` | Login as `client`. Creates a new client if not yet exists. |
| topup `<amount>` | Increase logged-in client balance by `amount`. |
| pay `<another_client>` `<amount>` | Pay `amount` from logged-in client to `another_client`,
maybe in parts, as soon as possible. |


## 'Pay' Command Examples


Given clients Harry and Tom with initial balances (100, 80):

| Action | Result Balances |
| -----------------------------| -----------------------------------------------------------------------------------------------|
| Tom pays Harry 50 | (150, 30) |
| Tom pays Harry 100 | (180, 0) with Tom owing 70 |
| Tom tops up 30 | (210, 0) with Tom owing 40 |
| Harry pays 30 to Tom | (210, 0) with Tom owing 10. Debt has further decreased |
| Tom tops up 100 | (220, 90) |


## Test Session
Console output of your implementation should contain as least all of the output of the following
scenario.
Feel free to add extra output as you see fit.

> login Harry
Hello, Harry!
Your balance is 0.

> topup 100
Your balance is 100.

> login Tom
Hello, Tom!
Your balance is 0.

> topup 80
Your balance is 80.

> pay Harry 50
Transferred 50 to Harry.
Your balance is 30.

> pay Harry 100
Transferred 30 to Harry.
Your balance is 0.
Owing 70 to Harry.

> topup 30
Transferred 30 to Harry.
Your balance is 0.
Owing 40 to Harry.

> login Harry
Hello, Harry!
Owing 40 from Tom.
Your balance is 210.

> pay Tom 30
Owing 10 from Tom.
Your balance is 210.

> login Tom
Hello, Tom!
Your balance is 0.
Owing 10 to Harry.

> topup 100
Transferred 10 to Harry.
Your balance is 90.

## Submission requirements
- Use OO , SOLID, pre-defined Design Patterns whenever needed in your code
- Write unit tests to cover your core code fully
- The design choices made when implementing the solution (maintainability, legibility, refactorability,extensibility.)
- Exception and special case handling
- Your code should handle edge cases and be covered with tests
- Clearly state instrutions and run requirements if any
- Provide your assumptions
- Please share the assignment via a private git repository
- We want to observe your commit messages and the process, please commit module by module, instead of committing at one shot
- Please add users as stated in the email as collaborators
